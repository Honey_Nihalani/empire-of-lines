﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Empire_Of_Line
{
    public sealed partial class Table : Page
    {
        public Table()
        {
            this.InitializeComponent();
            maintainScreenResoltion();
            story_P1.Begin();
            story_P1.Completed += story_P1_Completed;
            p1_score.Text = "0";
            p2_score.Text = "0";
            this.SizeChanged += smallTable_SizeChanged;
        }

        TextBlock textBlockType;
        Rectangle rectType;
        Image imgType;
        Button buttonType;
        double widthFector, heightFector;
        
        private void maintainScreenResoltion()
        {
            textBlockType = new TextBlock();
            rectType = new Rectangle();
            imgType = new Image();
            buttonType = new Button();
            widthFector = Window.Current.Bounds.Width / 1366;
            heightFector = Window.Current.Bounds.Height / 768;

            //TextBlocks
            setComponent(player1);
            setComponent(player2);
            setComponent(p1_score);
            setComponent(p2_score);
            setComponent(winner);

            //Rectangles
            setComponent(p1box1);
            setComponent(p1box2);
            setComponent(p2box1);
            setComponent(p2box2);

            //Image
            setComponent(g_over);
            setComponent(thBrown);
            setComponent(thDark);
            setComponent(thRetro);
            setComponent(thOldPaper);

            //Button
            setComponent(ctButton);

            //StackPanel
            stPanel.Height *= heightFector;

            //Appbar
            Prefernces.Height *= heightFector;

            //setwinningEffect
            goV1.Value = Window.Current.Bounds.Height * 1.03788 + 2.91;
            goV2.Value = Window.Current.Bounds.Height - 50;
            goV3.Value = Window.Current.Bounds.Height * 1.03788 + 2.91;
            goV4.Value = Window.Current.Bounds.Height - 25;
            goV5.Value = Window.Current.Bounds.Height * 1.03788 + 2.91;
            wV1.Value = goV1.Value;
            wV2.Value = goV2.Value;
            wV3.Value = goV3.Value;
            wV4.Value = goV4.Value;
            wV5.Value = goV5.Value;
        }
        
        private void setComponent(Object ele)
        {
            Thickness temp;
            if (ele.GetType() == textBlockType.GetType())
            {
                textBlockType = (TextBlock)ele;
                textBlockType.Width *= widthFector;
                textBlockType.Height *= heightFector;
                textBlockType.FontSize *= (heightFector / widthFector);
                temp = textBlockType.Margin;
                temp.Top *= heightFector;
                temp.Left *= widthFector;
                temp.Right *= widthFector;
                textBlockType.Margin = temp;
            }
            else if (ele.GetType() == rectType.GetType())
            {
                rectType = (Rectangle)ele;
                rectType.Width *= widthFector;
                rectType.Height *= heightFector;
                temp = rectType.Margin;
                temp.Top *= heightFector;
                temp.Left *= widthFector;
                rectType.Margin = temp;
            }
            else if(ele.GetType() == imgType.GetType())
            {
                imgType = (Image)ele;
                imgType.Width *= widthFector;
                imgType.Height *= heightFector;
                temp = imgType.Margin;
                temp.Left *= widthFector;
                temp.Top *= heightFector;
                imgType.Margin = temp;
            }
            else
            {
                buttonType = (Button)ele;
                buttonType.Width *= widthFector;
                buttonType.Height *= heightFector;
                buttonType.FontSize *= (heightFector / widthFector);
                temp = buttonType.Margin;
                temp.Top *= heightFector;
                temp.Left *= widthFector;
                buttonType.Margin = temp;
            }
        }

        void smallTable_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Windows.UI.ViewManagement.ApplicationView.Value == Windows.UI.ViewManagement.ApplicationViewState.FullScreenLandscape)
            {
                myGrid.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Fill.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Snap.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Port.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
            if (Windows.UI.ViewManagement.ApplicationView.Value == Windows.UI.ViewManagement.ApplicationViewState.Filled)
            {
                myGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Fill.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Snap.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Port.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
            if (Windows.UI.ViewManagement.ApplicationView.Value == Windows.UI.ViewManagement.ApplicationViewState.FullScreenPortrait)
            {
                myGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Fill.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Snap.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Port.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            if (Windows.UI.ViewManagement.ApplicationView.Value == Windows.UI.ViewManagement.ApplicationViewState.Snapped)
            {
                myGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Fill.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Snap.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Port.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
        }
        void story_P1_Completed(object sender, object e)
        {
            story_P1.Begin();
        }
        void story_P2_Completed(object sender, object e)
        {
            story_P2.Begin();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            object[,] data = e.Parameter as object[,];

            raw = (int)data[0, 0];
            col = (int)data[0, 1];

            p1name = (string)data[1, 0];
            p2name = (string)data[1, 1];

            p1char = p1name.Substring(0, 1).ToUpper();
            p2char = p2name.Substring(0, 1).ToUpper();

            genrateTable(raw, col);
        }

        int raw, col, p1counter, p2counter;
        TextBlock[,] block;
        Rectangle[, ,] rect;
        bool[, ,] status;
        Ellipse[,] ell;
        Rectangle backGroud;
        int[] index = new int[3];
        bool changeTurn = true;
        string p1name, p2name, p1char, p2char;

        private void genrateTable(int raw, int col)
        {
            InitializeMyComponent();
            for (int i = 0; i <= raw; i++)
            {
                for (int j = 0; j <= col; j++)
                {
                    EllipseGeneration(i, j);
                    if (!(j == col))
                    {
                        HorizontalRectangle(i, j);
                        if (!(i == raw))
                            blockGeneration(i, j);
                    }
                    if (!(i == raw))
                        VerticalRectangle(i, j);
                }
            }
        }

        private void InitializeMyComponent()
        {
            backGroud = new Rectangle();
            backGroud.Height = ((80 * raw) + 55) * heightFector;
            backGroud.Width = ((80 * col) + 60) * widthFector;
            backGroud.Fill = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 201, 134, 43));
            backGroud.Stroke = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 64, 0, 0));
            backGroud.StrokeThickness = 20;
            backGroud.Margin = new Thickness(0, -80 * (8 - raw) * heightFector, 0, 0);
            myGrid.Children.Add(backGroud);

            player1.Text = p1name;
            player2.Text = p2name;
            ell = new Ellipse[raw + 1, col + 1];
            rect = new Rectangle[2, raw + 1, col + 1];
            status = new bool[2, raw + 1, col + 1];
            block = new TextBlock[raw + 1, col + 1];
        }
        private void EllipseGeneration(int i, int j)
        {
            ell[i, j] = new Ellipse();
            ell[i, j].Height = 20 * heightFector;
            ell[i, j].Width = 20 * widthFector;
            ell[i, j].Fill = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 255, 255));
            ell[i, j].Margin = new Thickness(80 * (2 * j - col) * widthFector, 160 * (i - 4) * heightFector, 0, 0);
            myGrid.Children.Add(ell[i, j]);
        }
        private void HorizontalRectangle(int i, int j)
        {
            status[0, i, j] = new bool();
            status[0, i, j] = false;

            rect[0, i, j] = new Rectangle();
            rect[0, i, j].Height = 10 * heightFector;
            rect[0, i, j].Width = 60 * widthFector;
            rect[0, i, j].Fill = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0, 0, 0));
            rect[0, i, j].Opacity = 0.2;
            rect[0, i, j].Margin = new Thickness(80 * (2 * j - col + 1) * widthFector, 160 * (i - 4) * heightFector, 0, 0);
            int[] temp = { 0, i, j };
            rect[0, i, j].Tag = temp;
            rect[0, i, j].PointerPressed += Rectangle_PointerPressed;
            myGrid.Children.Add(rect[0, i, j]);
        }
        private void VerticalRectangle(int i, int j)
        {
            status[1, i, j] = new bool();
            status[1, i, j] = false;

            rect[1, i, j] = new Rectangle();
            rect[1, i, j].Height = 60 * heightFector;
            rect[1, i, j].Width = 10 * widthFector;
            rect[1, i, j].Fill = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0, 0, 0));
            rect[1, i, j].Opacity = 0.2;
            rect[1, i, j].Margin = new Thickness(80 * (2 * j - col) * widthFector, 80 * (2 * i - 7) * heightFector, 0, 0);
            int[] temp = { 1, i, j };
            rect[1, i, j].Tag = temp;
            rect[1, i, j].PointerPressed += Rectangle_PointerPressed;
            myGrid.Children.Add(rect[1, i, j]);
        }
        private void blockGeneration(int i, int j)
        {
            block[i, j] = new TextBlock();
            block[i, j].Height = 60 * heightFector;
            block[i, j].Width = 60 * widthFector;
            block[i, j].FontSize = 60;
            double minusFector60 = ((Window.Current.Bounds.Width - 1366) + (Window.Current.Bounds.Height - 768)) / 2969.4323;
            block[i, j].FontSize *= ((Window.Current.Bounds.Width * Window.Current.Bounds.Height) / (1366 * 768) - minusFector60);
            block[i, j].TextAlignment = TextAlignment.Center;
            block[i, j].Margin = new Thickness(80 * (2 * j - col + 1) * widthFector, 160 * (i - 3.5) * heightFector, 0, 0);
            myGrid.Children.Add(block[i, j]);
        }

        void Rectangle_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            Rectangle temp = (Rectangle)e.OriginalSource;
            index = (int[])temp.Tag;
            if (!status[index[0], index[1], index[2]])
            {
                status[index[0], index[1], index[2]] = true;
                temp.Opacity = 1;
                if (changeTurn)
                    temp.Fill = player1.Foreground;
                else
                    temp.Fill = player2.Foreground;

                putLine.Position = new TimeSpan(0);
                putLine.Play();

                if (index[0] == 0)
                    myRulesHori(index[1], index[2]);
                else
                    myRulesVerti(index[1], index[2]);
            }
            allow = true;
        }

        bool allow = true;
        private void myRulesHori(int i, int j)
        {
            if (i == 0)
            {
                if (status[0, i + 1, j] && status[1, i, j] && status[1, i, j + 1])
                    setP1orP2(block[i, j]);
                else
                    changeTurn = !changeTurn;
            }
            else if (i == raw)
            {
                if (status[0, i - 1, j] && status[1, i - 1, j] && status[1, i - 1, j + 1])
                    setP1orP2(block[i - 1, j]);
                else
                    changeTurn = !changeTurn;
            }
            else if (i >= j)
            {
                if (status[0, i - 1, j] && status[1, i - 1, j] && status[1, i - 1, j + 1])
                {
                    setP1orP2(block[i - 1, j]);
                    allow = false;
                }
                if (status[0, i + 1, j] && status[1, i, j] && status[1, i, j + 1])
                {
                    setP1orP2(block[i, j]);
                    allow = false;
                }
                if (allow)
                    changeTurn = !changeTurn;
            }
            else
            {
                if (status[0, i - 1, j] && status[1, i - 1, j] && status[1, i - 1, j + 1])
                {
                    setP1orP2(block[i - 1, j]);
                    allow = false;
                }
                if (status[0, i + 1, j] && status[1, i, j] && status[1, i, j + 1])
                {
                    setP1orP2(block[i, j]);
                    allow = false;
                }
                if (allow)
                    changeTurn = !changeTurn;
            }

            statusOfGame();
        }
        private void myRulesVerti(int i, int j)
        {
            if (j == 0)
            {
                if (status[1, i, j + 1] && status[0, i, j] && status[0, i + 1, j])
                    setP1orP2(block[i, j]);
                else
                    changeTurn = !changeTurn;
            }
            else if (j == col)
            {
                if (status[1, i, j - 1] && status[0, i, j - 1] && status[0, i + 1, j - 1])
                    setP1orP2(block[i, j - 1]);
                else
                    changeTurn = !changeTurn;
            }
            else if (j >= i)
            {
                if (status[1, i, j - 1] && status[0, i, j - 1] && status[0, i + 1, j - 1])
                {
                    setP1orP2(block[i, j - 1]);
                    allow = false;
                }
                if (status[1, i, j + 1] && status[0, i, j] && status[0, i + 1, j])
                {
                    setP1orP2(block[i, j]);
                    allow = false;
                }
                if (allow)
                    changeTurn = !changeTurn;
            }
            else
            {
                if (status[1, i, j - 1] && status[0, i, j - 1] && status[0, i + 1, j - 1])
                {
                    setP1orP2(block[i, j - 1]);
                    allow = false;
                }
                if (status[1, i, j + 1] && status[0, i, j] && status[0, i + 1, j])
                {
                    setP1orP2(block[i, j]);
                    allow = false;
                }
                if (allow)
                    changeTurn = !changeTurn;
            }

            statusOfGame();
        }

        private void setP1orP2(TextBlock block)
        {
            if (changeTurn)
            {
                block.Foreground = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 74, 39, 216));
                block.Text = p1char;
                p1counter++;
            }
            else
            {
                block.Foreground = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 63, 1, 1));
                block.Text = p2char;
                p2counter++;
            }
            if (!emp.IsMuted)
            {
                emp.Position = new TimeSpan(0);
                emp.Play();
            }
            else
                emp.Play();

            p1_score.Text = p1counter.ToString();
            p2_score.Text = p2counter.ToString();
        }
        private void statusOfGame()
        {
            if (changeTurn)
            {
                story_P2.Stop();
                story_P1.Begin();
                story_P1.Completed += story_P1_Completed;
            }
            else
            {
                story_P1.Stop();
                story_P2.Begin();
                story_P2.Completed += story_P2_Completed;
            }
            if ((p1counter + p2counter) == raw * col)
            {
                hideTable();

                story_P1.Stop();
                story_P2.Stop();

                if (p1counter > p2counter)
                    winner.Text = "!!.." + p1name + "..!!";
                else
                    winner.Text = "!!.." + p2name + "..!!";

                winningEffect.Begin();
            }
        }
        private void hideTable()
        {
            myGrid.Children.Remove(backGroud);
            for (int i = 0; i <= raw; i++)
            {
                for (int j = 0; j <= col; j++)
                {
                    myGrid.Children.Remove(ell[i, j]);
                    if (!(j == col))
                    {
                        myGrid.Children.Remove(rect[0, i, j]);
                        if (!(i == raw))
                            myGrid.Children.Remove(block[i, j]);
                    }
                    if (!(i == raw))
                        myGrid.Children.Remove(rect[1, i, j]);
                }
            }
        }

        private void themeChanged(object sender, PointerRoutedEventArgs e)
        {
            if (sender.Equals(thBrown))
            {
                backGroud.Fill = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 201, 134, 43));
                backGroud.Stroke = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 64, 0, 0));
                img.ImageSource = new BitmapImage(new Uri(@"ms-appx:///images/Theme/Brown.jpg", UriKind.RelativeOrAbsolute));
            }
            else if (sender.Equals(thDark))
            {
                backGroud.Fill = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 134, 134, 134));
                backGroud.Stroke = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0, 0, 0));
                img.ImageSource = new BitmapImage(new Uri(@"ms-appx:///images/Theme/Dark.png", UriKind.RelativeOrAbsolute));
            }
            else if (sender.Equals(thRetro))
            {
                backGroud.Fill = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 210, 102, 11));
                backGroud.Stroke = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 149, 14, 8));
                img.ImageSource = new BitmapImage(new Uri(@"ms-appx:///images/Theme/Retro.jpg", UriKind.RelativeOrAbsolute));
            }
            else
            {
                backGroud.Fill = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 169, 125, 64));
                backGroud.Stroke = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 76, 41, 13));
                img.ImageSource = new BitmapImage(new Uri(@"ms-appx:///images/Theme/Old_paper.jpg", UriKind.RelativeOrAbsolute));
            }
        }
        
        private void change_table_click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }
      
        private void soundStateChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                putLine.IsMuted = !putLine.IsMuted;
                emp.IsMuted = !emp.IsMuted;
            }
            catch (NullReferenceException nRe)
            {
            }
        }

    }
}