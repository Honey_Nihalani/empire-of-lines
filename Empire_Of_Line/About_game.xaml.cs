﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Empire_Of_Line
{
    public sealed partial class About_game : Page
    {
        public About_game()
        {
            this.InitializeComponent();
            maintainScreenResoltion();
        }

        Button buttonType;
        TextBlock textBlockType;
        Ellipse ellipseType;
        Image imgType;
        double widthFector, heightFector;
        private void maintainScreenResoltion()
        {
            buttonType = new Button();
            textBlockType = new TextBlock();
            ellipseType = new Ellipse();
            imgType = new Image();
            widthFector = Window.Current.Bounds.Width / 1366;
            heightFector = Window.Current.Bounds.Height / 768;

            setComponent(BackButton);
            setComponent(img);
            setComponent(id1);
            setComponent(id2);
            setComponent(id3);
            setComponent(id4);
            setComponent(p11);
            setComponent(p12);
            setComponent(p13);
            setComponent(p21);
            setComponent(p22);
            setComponent(p31);
            setComponent(p32);
            setComponent(p33);
            setComponent(p41);
            setComponent(p42Button);
            setComponent(preImg);
            setComponent(nextImg);
            setComponent(fb);
            setComponent(fbID);
            setComponent(em);
            setComponent(emID);
        }
        private void setComponent(Object ele)
        {
            Thickness temp;

            if (ele.GetType() == buttonType.GetType())
            {
                buttonType = (Button)ele;
                buttonType.Width *= widthFector;
                buttonType.Height *= heightFector;
                buttonType.FontSize *= (heightFector / widthFector);
                temp = buttonType.Margin;
                temp.Top *= heightFector;
                temp.Left *= widthFector;
                buttonType.Margin = temp;
            }
            else if (ele.GetType() == textBlockType.GetType())
            {
                textBlockType = (TextBlock)ele;
                textBlockType.Width *= widthFector;
                textBlockType.Height *= heightFector;
                textBlockType.FontSize *= widthFector;
                temp = textBlockType.Margin;
                temp.Top *= heightFector;
                temp.Left *= widthFector;
                textBlockType.Margin = temp;
            }
            else if (ele.GetType() == ellipseType.GetType())
            {
                ellipseType = (Ellipse)ele;
                ellipseType.Width *= widthFector;
                ellipseType.Height *= heightFector;
                temp = ellipseType.Margin;
                temp.Top *= heightFector;
                temp.Left *= widthFector;
                ellipseType.Margin = temp;
            }
            else
            {
                imgType = (Image)ele;
                imgType.Width *= widthFector;
                imgType.Height *= heightFector;
                temp = imgType.Margin;
                temp.Top *= heightFector;
                temp.Left *= widthFector;
                imgType.Margin = temp;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        int currentPage = 1;
        private void next_button_click(object sender, RoutedEventArgs e)
        {
            currentPage++;
            if (currentPage == 5)
            {
                currentPage--;
                //show msg that no next page is available.
            }
            switch (currentPage)
            {
                case 2:
                    hidePage1();
                    showPage2();
                    break;
                case 3:
                    hidePage2();
                    showPage3();
                    break;
                case 4:
                    hidePage3();
                    showPage4();
                    break;
            }
        }
        private void prev_button_click(object sender, RoutedEventArgs e)
        {
            currentPage--;
            if (currentPage == 0)
            {
                currentPage++;
                //show msg that no previous page is there
            }
            switch (currentPage)
            {
                case 1:
                    hidePage2();
                    showPage1();
                    break;
                case 2:
                    hidePage3();
                    showPage2();
                    break;
                case 3:
                    hidePage4();
                    showPage3();
                    break;
            }
        }

        private void hidePage1()
        {
            p11.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            p12.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            p13.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }
        private void hidePage2()
        {
            p21.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            p22.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }
        private void hidePage3()
        {
            p31.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            p32.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            p33.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }
        private void hidePage4()
        {
            p41.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            p42Button.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void showPage1()
        {
            id1.Opacity = 1;
            id2.Opacity = 0.5;
            id3.Opacity = 0.5;
            id4.Opacity = 0.5;

            p11.Visibility = Windows.UI.Xaml.Visibility.Visible;
            p12.Visibility = Windows.UI.Xaml.Visibility.Visible;
            p13.Visibility = Windows.UI.Xaml.Visibility.Visible;

            img.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/p1.png", UriKind.RelativeOrAbsolute));
        }
        private void showPage2()
        {
            id1.Opacity = 0.5;
            id2.Opacity = 1;
            id3.Opacity = 0.5;
            id4.Opacity = 0.5;

            p21.Visibility = Windows.UI.Xaml.Visibility.Visible;
            p22.Visibility = Windows.UI.Xaml.Visibility.Visible;

            img.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/p2.png", UriKind.RelativeOrAbsolute));
        }
        private void showPage3()
        {
            id1.Opacity = 0.5;
            id2.Opacity = 0.5;
            id3.Opacity = 1;
            id4.Opacity = 0.5;

            p31.Visibility = Windows.UI.Xaml.Visibility.Visible;
            p32.Visibility = Windows.UI.Xaml.Visibility.Visible;
            p33.Visibility = Windows.UI.Xaml.Visibility.Visible;

            img.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/p3.png", UriKind.RelativeOrAbsolute));
        }
        private void showPage4()
        {
            id1.Opacity = 0.5;
            id2.Opacity = 0.5;
            id3.Opacity = 0.5;
            id4.Opacity = 1;

            p41.Visibility = Windows.UI.Xaml.Visibility.Visible;
            p42Button.Visibility = Windows.UI.Xaml.Visibility.Visible;
            img.Source = new BitmapImage(new Uri(@"ms-appx:///images/About_game/p4.png", UriKind.RelativeOrAbsolute));
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }
        private void lets_start_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        private void ellipseClicked(object sender, PointerRoutedEventArgs e)
        {
            if (sender.Equals(id1))
            {
                currentPage = 1;
                showPage1();
                hidePage2();
                hidePage3();
                hidePage4();
            }
            else if (sender.Equals(id2))
            {
                currentPage = 2;
                showPage2();
                hidePage1();
                hidePage3();
                hidePage4();
            }
            else if (sender.Equals(id3))
            {
                currentPage = 3;
                showPage3();
                hidePage2();
                hidePage1();
                hidePage4();
            }
            else
            {
                currentPage = 4;
                showPage4();
                hidePage2();
                hidePage3();
                hidePage1();
            }
        }

    }
}