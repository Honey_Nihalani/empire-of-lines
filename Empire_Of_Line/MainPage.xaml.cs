﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Empire_Of_Line
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            maintainScreenResoltion();
            Story_all.Begin();
            Story_all.Completed += Story_all_Completed;
        }

        Button buttonType;
        TextBlock textBlockType;
        TextBox textBoxType;
        ComboBox comboType;
        double widthFector, heightFector;
        private void maintainScreenResoltion()
        {
            buttonType = new Button();
            textBlockType = new TextBlock();
            textBoxType = new TextBox();
            comboType = new ComboBox();

            widthFector = Window.Current.Bounds.Width / 1366;
            heightFector = Window.Current.Bounds.Height / 768;

            setComponent(SmallButton);
            setComponent(MiddleButton);
            setComponent(RandomButton);
            setComponent(CustomButton);
            setComponent(playButton);
            setComponent(About_game);

            setComponent(labelP1);
            setComponent(labelP2);
            setComponent(getP1);
            setComponent(getP2);

            setComponent(tbLabel);
            setComponent(raw);
            setComponent(x);
            setComponent(col);

            //setSelectTabelEffect
            sbV.Value = 266 - Window.Current.Bounds.Width;
            mbV.Value = 266 - Window.Current.Bounds.Width;
            rbV.Value = 266 - Window.Current.Bounds.Width;
            cbV.Value = 266 - Window.Current.Bounds.Width;

            //setAskForPlayerName
            g1V.Value = 286 - Window.Current.Bounds.Width;
            g2V.Value = 286 - Window.Current.Bounds.Width;
            l1V.Value = 286 - Window.Current.Bounds.Width;
            l2V.Value = 286 - Window.Current.Bounds.Width;
            pbV.Value = 141 - Window.Current.Bounds.Width;
            agV.Value = 141 - Window.Current.Bounds.Width;

            //setAskForRawCol
            tlV.Value = 296 - Window.Current.Bounds.Width;
            rV.Value = 216 - Window.Current.Bounds.Width;
            cV.Value = 216 - Window.Current.Bounds.Width;
            xV.Value = 216 - Window.Current.Bounds.Width;
        }
        private void setComponent(Object ele)
        {            
            Thickness temp;

            if (ele.GetType() == buttonType.GetType())
            {
                buttonType = (Button)ele;
                buttonType.Width *= widthFector;
                buttonType.Height *= heightFector;
                buttonType.FontSize *= (heightFector / widthFector);
                temp = buttonType.Margin;
                temp.Top *= heightFector;
                temp.Left *= widthFector;
                temp.Right *= widthFector;
                buttonType.Margin = temp;
            }
            else if (ele.GetType() == textBlockType.GetType())
            {
                textBlockType = (TextBlock)ele;
                textBlockType.Width *= widthFector;
                textBlockType.Height *= heightFector;
                textBlockType.FontSize *= (heightFector / widthFector);
                temp = textBlockType.Margin;
                temp.Top *= heightFector;
                temp.Left *= widthFector;
                temp.Right *= widthFector;
                textBlockType.Margin = temp;
            }
            else if (ele.GetType() == textBoxType.GetType())
            {
                textBoxType = (TextBox)ele;
                textBoxType.Width *= widthFector;
                textBoxType.Height *= heightFector;
                textBoxType.FontSize *= (heightFector / widthFector);
                temp = textBoxType.Margin;
                temp.Top *= heightFector;
                temp.Left *= widthFector;
                temp.Right *= widthFector;
                textBoxType.Margin = temp;
            }
            else
            {
                comboType = (ComboBox)ele;
                comboType.Width *= widthFector;
                comboType.Height *= heightFector;
                comboType.FontSize *= (heightFector / widthFector);
                temp = comboType.Margin;
                temp.Top *= heightFector;
                temp.Left *= widthFector;
                temp.Right *= widthFector;
                comboType.Margin = temp;
            }
        }
        
        void Story_all_Completed(object sender, object e)
        {
            Story_all.Begin();
        }
        void table_selected_Completed(object sender, object e)
        {
            if (neviTo == 'C')
                AskForRawCol.Begin();
            AskForPlayerName.Begin();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

        }

        char neviTo;
        private void SmallButton_Click_1(object sender, RoutedEventArgs e)
        {
            ButtonClick('S');
        }
        private void MiddleButton_Click_1(object sender, RoutedEventArgs e)
        {
            ButtonClick('M');
        }
        private void RandomButton_Click_1(object sender, RoutedEventArgs e)
        {
            ButtonClick('R');
        }
        private void CustomButton_Click_1(object sender, RoutedEventArgs e)
        {
            ButtonClick('C');
        }
        private void ButtonClick(char table)
        {
            click.Play();
            neviTo = table;
            table_selected.Begin();
            table_selected.Completed += table_selected_Completed;
        }

        private void SmallButton_PointerEntered_1(object sender, PointerRoutedEventArgs e)
        {
            pointerEnteredInButton(SmallButton);
        }
        private void MiddleButton_PointerEntered_1(object sender, PointerRoutedEventArgs e)
        {
            pointerEnteredInButton(MiddleButton);
        }
        private void RandomButton_PointerEntered_1(object sender, PointerRoutedEventArgs e)
        {
            pointerEnteredInButton(RandomButton);
        }
        private void CustomButton_PointerEntered_1(object sender, PointerRoutedEventArgs e)
        {
            pointerEnteredInButton(CustomButton);
        }
        private void pointerEnteredInButton(Button myButton)
        {
            click.Play();
            myButton.BorderBrush = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 255, 255));
            Story_all.Stop();
        }

        private void SmallButton_PointerExited_1(object sender, PointerRoutedEventArgs e)
        {
            pointerExitedFromButton(SmallButton);
        }
        private void MiddleButton_PointerExited_1(object sender, PointerRoutedEventArgs e)
        {
            pointerExitedFromButton(MiddleButton);
        }
        private void RandomButton_PointerExited_1(object sender, PointerRoutedEventArgs e)
        {
            pointerExitedFromButton(RandomButton);
        }
        private void CustomButton_PointerExited_1(object sender, PointerRoutedEventArgs e)
        {
            pointerExitedFromButton(CustomButton);
        }
        private void pointerExitedFromButton(Button myButton)
        {
            myButton.BorderBrush = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 24, 14, 126));
            Story_all.Begin();
        }

        int[] rawCol = new int[2];
        string[] p1p2 = new string[2];

        private void Play_Button_Click(object sender, RoutedEventArgs e)
        {
            object[,] data = new object[2, 2];
            p1p2[0] = getP1.Text;
            p1p2[1] = getP2.Text;

            if (neviTo == 'S')
            {
                rawCol[0] = 6;
                rawCol[1] = 6;
            }
            else if (neviTo == 'M')
            {
                rawCol[0] = 7;
                rawCol[1] = 10;
            }
            else if (neviTo == 'R')
            {
                Random num = new Random();
                rawCol[0] = num.Next(5, 8);
                rawCol[1] = num.Next(5, 16);
            }
            else
            {
                rawCol[0] = custRaw + 5;
                rawCol[1] = custCol + 8;
            }

            data[0, 0] = rawCol[0];
            data[0, 1] = rawCol[1];
            data[1, 0] = p1p2[0];
            data[1, 1] = p1p2[1];
            this.Frame.Navigate(typeof(Table), data);
        }

        private void About_game_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(About_game));
        }

        int custRaw, custCol;

        private void raw_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                custRaw = raw.SelectedIndex;
            }
            catch (NullReferenceException nRe)
            {

            }
        }
        private void col_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                custCol = col.SelectedIndex;
            }
            catch (NullReferenceException nre)
            {

            }
        }

        private void playButton_PointerEntered_1(object sender, PointerRoutedEventArgs e)
        {
            click.Play();
        }
        private void About_game_PointerEntered_1(object sender, PointerRoutedEventArgs e)
        {
            click.Play();
        }
    }
}